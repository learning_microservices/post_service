FROM golang:1.20-alpine
RUN mkdir post
COPY . /post
WORKDIR /post
RUN go mod tidy && go mod vendor
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 8010

# Set environment variables
# ENV POSTGRES_USER=postgres
# ENV POSTGRES_PASSWORD=1234
# ENV POSTGRES_HOST=database
# ENV POSTGRES_PORT=5434
# ENV POSTGRES_DATABASE=user_db
# ENV ENVIRONMENT=develop
# ENV LOG_LEVEL=debug
# ENV USER_SERVICE_HOST=user_service
# ENV USER_SERVICE_PORT=:7000
# ENV POST_SERVICE_HOST=post_service
# ENV POST_SERVICE_PORT=

