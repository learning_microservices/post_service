package repo

import (
	"context"

	p "gitlab.com/micro/post_service/genproto/post"
)

type PostStorageI interface {
	CreatePost(*p.PostRequest) (*p.GetPostResponse, error)
	GetPostById(context.Context, *p.IdRequest) (*p.GetPostResponse, error)
	GetPostByUserId(context.Context, *p.IdUser) (*p.Posts, error)
	GetPostForUser(context.Context, *p.IdUser) (*p.Posts, error)
	GetPostForComment(context.Context, *p.IdRequest) (*p.GetPostResponse, error)
	SearchByTitle(context.Context, *p.Title) (*p.Posts, error)
	LikePost(context.Context, *p.LikeRequest) (*p.GetPostResponse, error)
	UpdatePost(context.Context, *p.UpdatePostRequest) error
	DeletePost(context.Context, *p.IdRequest) (*p.GetPostResponse, error)
}
